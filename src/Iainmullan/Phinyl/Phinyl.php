<?php

$autoload = dirname(__FILE__).'/../../../vendor/autoload.php';
include($autoload);

use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Cache\CacheSubscriber;
use GuzzleHttp\Subscriber\Cache\CacheStorage;
use GuzzleHttp\Subscriber\Cache\DefaultCacheStorage;

use Doctrine\Common\Cache\FilesystemCache;

class Phinyl {

    const ENV_DEV = 0;
    const ENV_LIVE = 2;

    private $client;
    public $response;

    function __construct($apiKey, $cacheDir = false, $cacheLength = 3600, $env = Phinyl::ENV_LIVE) {

        if ($env == static::ENV_DEV) {
            $baseUrl = 'http://api.vs.loc/';
        } else if ($env == static::ENV_LIVE) {
            $baseUrl = 'https://api.vinylscrobbler.com/';
        }

        $this->client = new Client([
            'base_url' => [$baseUrl, []],
            'defaults' => [
                'query'  => ['session_key' => $apiKey, 'format' => 'json']
            ]
        ]);
        $this->client->setDefaultOption('verify', false);

        if ($cacheDir !== false) {

            $storage = new CacheStorage(
                new FilesystemCache($cacheDir), '.phinyl', $cacheLength
            );

            CacheSubscriber::attach($this->client, array(
                'storage' => $storage,
            ));

        }

    }

    public function get($endpoint, $params = array()) {

        $request = $this->client->createRequest('GET', $endpoint, [
            'query' => $params
        ]);

        $this->response = $this->client->send($request);

        $data = $this->response->getBody();

        if (!$data) {
            return FALSE;
        }

        $data = json_decode($data, true);

        return $data;
    }

    public function post($endpoint, $params = array()) {

        $request = $this->client->createRequest('POST', $endpoint, [
            'query' => $params
        ]);

        $this->response = $this->client->send($request);

        $data = $this->response->getBody();

        if (!$data) {
            return FALSE;
        }

        $data = json_decode($data, true);

        return $data;
    }

}
